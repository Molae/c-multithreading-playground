//
// Created by Milton on 01-May-22.
//

#ifndef C_MULTITHREADING_PLAYGROUND_PROCESSOR_INFO_H
#define C_MULTITHREADING_PLAYGROUND_PROCESSOR_INFO_H

#include <Windows.h>
#include <malloc.h>
#include <stdio.h>

typedef BOOL (WINAPI *LPFN_GLPI)(
        PSYSTEM_LOGICAL_PROCESSOR_INFORMATION,
        PDWORD);

// Helper function to count set bits in the processor mask.
int CountSetBits(ULONG_PTR bitMask) {
    int LSHIFT = sizeof(ULONG_PTR) * 8 - 1;
    int bitSetCount = 0;
    ULONG_PTR bitTest = (ULONG_PTR) 1 << LSHIFT;
    int i;

    for (i = 0; i <= LSHIFT; ++i) {
        bitSetCount += ((bitMask & bitTest) ? 1 : 0);
        bitTest /= 2;
    }

    return bitSetCount;
}

int getAmountLogicalProcessors() {
    LPFN_GLPI glpi;
    int done = 0;
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
    PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
    DWORD returnLength = 0;
    int logicalProcessorCount = 0;
    int byteOffset = 0;

    glpi = (LPFN_GLPI) GetProcAddress(GetModuleHandle("kernel32"), "GetLogicalProcessorInformation");
    if (glpi == NULL) {
        printf("\nGetLogicalProcessorInformation is not supported.\n");
        return (1);
    }

    while (!done) {
        int rc = glpi(buffer, &returnLength);

        if (rc == 0) {
            if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                if (buffer) {
                    free(buffer);
                }

                buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(returnLength);

                if (buffer == NULL) {
                    printf("\nError: Allocation failure\n");
                    return (2);
                }
            } else {
                printf("\nError %lu\n", GetLastError());
                return (3);
            }
        } else {
            done = TRUE;
        }
    }

    ptr = buffer;

    while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= returnLength) {
        switch (ptr->Relationship) {
            case RelationProcessorCore:
                // A hyperthreaded core supplies more than one logical processor.
                logicalProcessorCount += CountSetBits(ptr->ProcessorMask);
                break;
            default:
                break;
        }
        byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
        ptr++;
    }

    free(buffer);

    return logicalProcessorCount;
}

#endif //C_MULTITHREADING_PLAYGROUND_PROCESSOR_INFO_H
