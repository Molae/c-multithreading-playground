#include "header.h"

#include <time.h>

int main() {
    int logicalProcessorCount = getAmountLogicalProcessors();

    HANDLE *threads = malloc(sizeof(HANDLE) * logicalProcessorCount);

    int result = 0;

    MYDATA *data = malloc(sizeof(MYDATA) * logicalProcessorCount);

    int *largeArray = malloc(sizeof(int) * LARGE_NUM);

    for (int i = 0; i < LARGE_NUM; i++) {
        largeArray[i] = i;
    }

    clock_t before = clock();
    for (int i = 0; i < logicalProcessorCount; i++) {
        data[i].values = largeArray;
        data[i].valuesOffset = i * (LARGE_NUM / logicalProcessorCount);
        data[i].valuesSize = LARGE_NUM / logicalProcessorCount;
        data[i].result = &result;

        threads[i] = CreateThread(NULL, 0, ThreadFunc, &data[i], 0, NULL);
    }
    WaitForMultipleObjects(logicalProcessorCount, threads, TRUE, INFINITE);

    for (int i = 0; i < logicalProcessorCount; i++) {
        CloseHandle(threads[i]);
    }

    free(threads);
    clock_t after = clock();

    printf("%d\n", result);
    printf("Time taken (multi-thread): %fs\n", (double)(after - before) / CLOCKS_PER_SEC);

    // setup
    result = 0;
    before = clock();

    // actual code
    for (int i = 0; i < LARGE_NUM; i++) {
        result += largeArray[i];
    }

    // time after sum
    after = clock();

    printf("%d\n", result);
    printf("Time taken (single-thread): %fs\n", (double)(after - before) / CLOCKS_PER_SEC);

    return 0;
}

DWORD WINAPI ThreadFunc(void *data) {
    MYDATA myData = *(MYDATA *) data;

    int result = 0;
    for (int i = myData.valuesOffset; i < myData.valuesOffset + myData.valuesSize; i++) {
        result += myData.values[i];
    }
    *myData.result += result;

    return 0;
}