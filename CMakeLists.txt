cmake_minimum_required(VERSION 3.22)
project(c_multithreading_playground C)

set(CMAKE_C_STANDARD 90)

add_executable(c_multithreading_playground main.c processor_info.h header.h)
