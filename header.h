//
// Created by Milton on 01-May-22.
//

#ifndef C_MULTITHREADING_PLAYGROUND_HEADER_H
#define C_MULTITHREADING_PLAYGROUND_HEADER_H

#define LARGE_NUM 1000000000

#include "processor_info.h"

DWORD WINAPI ThreadFunc(void* data);

typedef struct MyData {
    int *result;
    int valuesOffset;
    int valuesSize;
    int *values;
} MYDATA;

#endif //C_MULTITHREADING_PLAYGROUND_HEADER_H
